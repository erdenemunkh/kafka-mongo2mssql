#!/bin/bash

mongosh <<EOF
var config = {
    "_id": "rs1",
    "version": 1,
    "members": [
        {
            "_id": 0,
            "host": "localhost:27017",
        }
    ]
};
rs.initiate(config, { force: true });
rs.status();

rs.initiate({ "_id": "rs1", "version": 1, "members": [{ "_id": 0, "host": "localhost:27017" }]});

EOF